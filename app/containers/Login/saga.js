import { call, put, select, takeLatest } from 'redux-saga/effects';
import request from 'utils/request';
import { change } from 'redux-form';
import swal from 'sweetalert';
import { SIGN_IN, FORGOT_PASSWORD_EMAIL } from './constants';
import { signInError } from './actions';
import { signInSuccess } from '../App/actions';
// Individual exports for testing
import { showNoty, hideNoty } from '../SnackBarNoty/actions';
import { configuredStore } from '../../configureStore';

import { makeSelectSignInForm } from './selectors';

function* signIn() {
  const signInFormData = yield select(makeSelectSignInForm());

  try {
    const data = {
      email: signInFormData.email,
      password: signInFormData.password,
      deviceType: 'web',
    };
    const options = {
      method: 'POST',
      data,
      url: '/Members/municipalityRepresentativesSignIn',
    };
    const token = yield call(request, options);

    if (!token.roles) {
      swal({
        closeOnClickOutside: false,
        closeOnEsc: false,
        icon: 'error', // eslint-disable-next-line
        text: "Not a valid user"
      });
    }
    yield put(signInSuccess(token));
  } catch (e) {
    yield put(signInError(e));
    yield put(change('signIn', 'password', null));
  }
}

function* forgotPassword(action) {
  try {
    const data = {
      email: action.email,
      deviceType: 'web',
    };
    const options = {
      method: 'POST',
      data,
      url: '/Members/OperatorForgotPassword',
    };
    yield call(request, options);
    yield put(showNoty(`${action.message}`));
    setTimeout(() => {
      configuredStore.dispatch(hideNoty());
    }, 2000);
  } catch (e) {
    yield put(signInError(e));
    yield put(change('signIn', 'password', null));
  }
}

export default function* signInSaga() {
  // See example in containers/HomePage/saga.js
  yield takeLatest(SIGN_IN, signIn);
  yield takeLatest(FORGOT_PASSWORD_EMAIL, forgotPassword);
}
