/*
 *
 * Login constants
 *
 */

export const DEFAULT_ACTION = 'app/Login/DEFAULT_ACTION';
export const SIGN_IN = 'app/Login/SIGN_IN';
export const SIGN_IN_ERROR = 'app/Login/SIGN_IN_ERROR';
export const FORGOT_PASSWORD_EMAIL = 'app/Login/FORGOT_PASSWORD_EMAIL';
