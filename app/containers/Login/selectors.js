import { createSelector } from 'reselect';
import { initialState } from './reducer';

/**
 * Direct selector to the login state domain
 */

const selectLoginDomain = state => state.login || initialState;
const selectSignInForm = state => state.form || initialState;
/**
 * Other specific selectors
 */

/**
 * Default selector used by Login
 */

const makeSelectLogin = () =>
  createSelector(
    selectLoginDomain,
    substate => substate,
  );

const makeSelectLoginLoading = () =>
  createSelector(
    selectLoginDomain,
    substate => substate.loading,
  );

const makeSelectSignInForm = () =>
  createSelector(
    selectSignInForm,
    substate =>
      substate
        .get('signIn')
        .get('values')
        .toJS(),
  );

export default makeSelectLogin;
export {
  selectLoginDomain,
  makeSelectLogin,
  makeSelectLoginLoading,
  makeSelectSignInForm,
};
