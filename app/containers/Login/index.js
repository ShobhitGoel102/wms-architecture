/**
 *
 * Login
 *
 */

import React, { memo, useState } from 'react';

import PropTypes from 'prop-types';
import { injectIntl, FormattedMessage } from 'react-intl';
import { connect } from 'react-redux';

import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';
import { Field, reduxForm } from 'redux-form/immutable';
import { useInjectSaga } from 'utils/injectSaga';
import { useInjectReducer } from 'utils/injectReducer';

import IconButton from '@material-ui/core/IconButton';
import { CircularProgress } from '@material-ui/core';
// import Email from '@material-ui/icons/Email';
import InputAdornment from '@material-ui/core/InputAdornment';
import TextField from '@material-ui/core/TextField';
import Visibility from '@material-ui/icons/Visibility';
import VisibilityOff from '@material-ui/icons/VisibilityOff';
// @material-ui/core components
import withStyles from '@material-ui/core/styles/withStyles';

import Dialog from '@material-ui/core/Dialog';
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogContent from '@material-ui/core/DialogContent';
import DialogActions from '@material-ui/core/DialogActions';

import _ from 'lodash';
import messages from './messages';
import saga from './saga';
import reducer from './reducer';
import { makeSelectLogin, makeSelectLoginLoading } from './selectors';

// core components
import GridContainer from '../../library/components/Grid/GridContainer';
import GridItem from '../../library/components/Grid/GridItem';
import Button from '../../library/components/CustomButtons/Button';
import Card from '../../library/components/Card/Card';
import CardBody from '../../library/components/Card/CardBody';
import CardHeader from '../../library/components/Card/CardHeader';

import loginPageStyle from '../../library/assets/jss/material-dashboard-pro-react/views/loginPageStyle';
import './injectStyles.css';
import Input from '../../components/Input';
import { signIn, forgotPasswordEmail } from './actions';

const checkEmail = /\S+@\S+\.\S+/;

export function Login(props) {
  useInjectReducer({ key: 'login', reducer });
  useInjectSaga({ key: 'login', saga });

  const { handleSubmit, signInForm, classes, pristine, loading } = props;

  const [values, setValues] = React.useState({
    password: '',
    showPassword: true,
  });

  const handleChange = prop => event => {
    setValues({ ...values, [prop]: event.target.value });
  };

  const handleClickShowPassword = () => {
    setValues({ ...values, showPassword: !values.showPassword });
  };

  const [smallModal, setSmallModal] = useState(false);
  const [email, setEmail] = useState('');

  return (
    <div
      className="d-flex flex-column align-items-center justify-content-between w-100 h-100"
      style={{ width: '100vw', overflow: 'hidden' }}
    >
      <div className={classes.content}>
        <div className={classes.container}>
          <form onSubmit={handleSubmit(signInForm)}>
            <GridContainer justify="center">
              <GridItem>
                <Card login>
                  <CardHeader
                    className={`${classes.cardHeader} ${classes.textCenter}`}
                    color="rose"
                    id="changeColor"
                  >
                    <h4 className={classes.cardTitle}>
                      <FormattedMessage {...messages.loginHeader} />
                    </h4>
                  </CardHeader>
                  <CardBody>
                    <Field
                      name="email"
                      id="inputColor"
                      component={Input}
                      label={props.intl.formatMessage(messages.email)}
                      style={{
                        fontWeight: '100',
                        width: '90%',
                        marginLeft: '10px',
                      }}
                      InputProps={{
                        autoComplete: 'new-password',
                      }}
                    />

                    <Field
                      name="password"
                      type={values.showPassword ? 'password' : 'text'}
                      component={Input}
                      label={props.intl.formatMessage(messages.password)}
                      onChange={handleChange('password')}
                      style={{
                        fontWeight: '100',
                        width: '90%',
                        marginLeft: '10px',
                      }}
                      id="inputColor"
                      InputProps={{
                        endAdornment: (
                          <InputAdornment position="end">
                            <IconButton
                              edge="end"
                              aria-label="Toggle password visibility"
                              onClick={handleClickShowPassword}
                            >
                              {values.showPassword ? (
                                <VisibilityOff />
                              ) : (
                                <Visibility />
                              )}
                            </IconButton>
                          </InputAdornment>
                        ),
                        autoComplete: 'new-password',
                      }}
                    />
                    {loading ? (
                      <CircularProgress
                        color="secondary"
                        id="buttonColor"
                        style={{
                          height: '30px',
                          width: '30px',
                          marginLeft: '45%',
                          marginTop: '15px',
                          marginBottom: '20px',
                        }}
                      />
                    ) : (
                      <Button
                        color="rose"
                        simple
                        size="lg"
                        block
                        type="submit"
                        id="buttonColor"
                        disabled={pristine || loading}
                      >
                        <FormattedMessage {...messages.loginButton} />
                      </Button>
                    )}
                    <Button
                      color="rose"
                      simple
                      size="sm"
                      style={{ marginLeft: '50%' }}
                      onClick={() => setSmallModal(true)}
                      id="buttonColor"
                    >
                      <FormattedMessage {...messages.forgotPassword} />
                    </Button>
                  </CardBody>
                </Card>
              </GridItem>
            </GridContainer>

            <Dialog
              classes={{
                root: `${classes.center} ${classes.modalRoot}`,
                paper: `${classes.modal} ${classes.modalSmall}`,
              }}
              disableBackdropClick
              disableEscapeKeyDown
              open={smallModal}
              keepMounted
              onClose={() => setSmallModal(false)}
              aria-labelledby="small-modal-slide-title"
              aria-describedby="small-modal-slide-description"
            >
              <DialogTitle
                id="small-modal-slide-title"
                disableTypography
                className={classes.modalHeader}
              >
                <FormattedMessage {...messages.forgotPassword} />
              </DialogTitle>
              <DialogContent
                id="small-modal-slide-description"
                className={`${classes.modalBody} ${classes.modalSmallBody}`}
              >
                <TextField
                  id="email"
                  label={props.intl.formatMessage(messages.email)}
                  value={email}
                  onChange={event => setEmail(event.target.value)}
                />
              </DialogContent>
              <DialogActions
                className={`${classes.modalFooter} ${
                  classes.modalFooterCenter
                }`}
              >
                <Button
                  color="transparent"
                  className={classes.modalSmallFooterFirstButton}
                  onClick={() => {
                    setSmallModal(false);
                    setEmail('');
                  }}
                >
                  <FormattedMessage {...messages.cancel} />
                </Button>
                <Button
                  onClick={() => {
                    setSmallModal(false);
                    setEmail('');
                    props.forgotPassword(
                      email,
                      messages.forgotPasswordSuccess.defaultMessage,
                    );
                  }}
                  color="success"
                  simple
                  className={`${classes.modalSmallFooterFirstButton} ${
                    classes.modalSmallFooterSecondButton
                  }`}
                  disabled={_.isEmpty(email) || !checkEmail.test(email)}
                >
                  <FormattedMessage {...messages.send} />
                </Button>
              </DialogActions>
            </Dialog>
          </form>
        </div>
      </div>
    </div>
  );
}

Login.propTypes = {
  ...Login,
  dispatch: PropTypes.func,
};

const mapStateToProps = createStructuredSelector({
  login: makeSelectLogin(),
  loading: makeSelectLoginLoading(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
    signInForm: () => {
      dispatch(signIn());
    },
    forgotPassword: (email, message) => {
      dispatch(forgotPasswordEmail(email, message));
    },
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

const withCustomStyles = withStyles(loginPageStyle);

const validate = values => {
  const errors = {};

  // email validation
  if (!values.get('email')) {
    errors.email = true;
  } else if (values.get('email')) {
    const email = values.get('email');
    const re = /\S+@\S+\.\S+/;
    if (!re.test(email)) {
      errors.email = true;
    }
  }
  if (!values.get('password')) {
    errors.password = true;
  }
  return errors;
};

const LoginForm = reduxForm({
  form: 'signIn',
  validate,
  keepDirtyOnReinitialize: false,
  enableReinitialize: true,
})(injectIntl(Login));

export default compose(
  withConnect,
  memo,
  withCustomStyles,
)(LoginForm);
