/*
 * EmergencyDashboard Messages
 *
 * This contains all the text for the EmergencyDashboard container.
 */

import { defineMessages } from "react-intl";

export const scope = "app.containers.EmergencyDashboard";

export default defineMessages({
  header: {
    id: `${scope}.header`,
    defaultMessage: "This is the EmergencyDashboard container!"
  }
});
