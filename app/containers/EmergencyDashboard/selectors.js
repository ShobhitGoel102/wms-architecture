import { createSelector } from "reselect";
import { initialState } from "./reducer";

/**
 * Direct selector to the emergencyDashboard state domain
 */

const selectEmergencyDashboardDomain = state =>
  state.emergencyDashboard || initialState;

/**
 * Other specific selectors
 */

/**
 * Default selector used by EmergencyDashboard
 */

const makeSelectEmergencyDashboard = () =>
  createSelector(
    selectEmergencyDashboardDomain,
    substate => substate
  );

export default makeSelectEmergencyDashboard;
export { selectEmergencyDashboardDomain };
