/*
 * HomePage
 *
 * This is the first thing users see of our App, at the '/' route
 *
 */

import React from 'react';
import Login from '../Login/index';

export default function HomePage() {
  return <Login />;
}
