/**
 *
 * App.js
 *
 * This component is the skeleton around the actual pages, and should only
 * contain code that should be seen on all pages. (e.g. navigation bar)
 *
 */

import React from "react";
import { Switch, Route } from "react-router-dom";
import history from "utils/history";
import NotFoundPage from "containers/NotFoundPage/Loadable";
import LoginPage from "../Login/index";
import Root from "../Dashboard/index";
import SnackBarNoty from "../SnackBarNoty/index";
import GlobalStyle from "../../global-styles";

import {
  userIsAuthenticatedRedir,
  userIsNotAuthenticatedRedir,
} from "../../auth";

const SignIn = userIsNotAuthenticatedRedir(LoginPage);
const RootContainer = userIsAuthenticatedRedir(Root);

history.listen(() => {
  window.scrollTo(0, 0);
});

export default function App() {
  return (
    <div style={{ height: "100vh" }}>
      <SnackBarNoty />
      <Switch>
        <Route exact path="/login" component={SignIn} />
        <Route path="/" component={RootContainer} />
        <Route component={NotFoundPage} />
      </Switch>
      <GlobalStyle />
    </div>
  );
}
