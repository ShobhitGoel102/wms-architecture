/*
 *
 * Shobhit reducer
 *
 */
import produce from 'immer';
import { DEFAULT_ACTION, MUNICIPALITY } from './constants';

export const initialState = { persisted: false };

/* eslint-disable default-case, no-param-reassign */
const appReducer = (state = initialState, action) =>
  produce(state, draft => {
    switch (action.type) {
      case DEFAULT_ACTION:
        break;
      case MUNICIPALITY:
        draft.municipality = action.m;
        break;
    }
  });

export default appReducer;
