/**
 *
 * Dashboard
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { Helmet } from 'react-helmet';
import injectSaga from 'utils/injectSaga';
import injectReducer from 'utils/injectReducer';
import { createStructuredSelector } from 'reselect';
import {
  makeSelectDashboard,
  makeSelectBuildOrganisationPlan,
} from './selectors';
import reducer from './reducer';
import saga from './saga';
import DashboardTheme from './DashboardTheme';
import { getOrganisationPlan, getMember, unPaidParking } from './actions';
import { makeSelectSignInToken } from '../App/selectors';
import './injectStyles.scss';
export class Dashboard extends React.PureComponent {
  componentDidMount() {
    this.props.dispatch(getMember());
    setTimeout(() => {
      this.props.dispatch(getOrganisationPlan());
      this.props.dispatch(unPaidParking());
    }, 3000);
  }

  render() {
    return (
      <div>
        <Helmet>
          <title>Dashboard</title>
          <meta name="description" content="Description of Dashboard" />
        </Helmet>
        <DashboardTheme {...this.props} />
      </div>
    );
  }
}

Dashboard.propTypes = {
  dispatch: PropTypes.func.isRequired,
  history: PropTypes.object,
};

const mapStateToProps = createStructuredSelector({
  dashboard: makeSelectDashboard(),
  parking: makeSelectBuildOrganisationPlan(),
  token: makeSelectSignInToken(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

const withReducer = injectReducer({ key: 'dashboard', reducer });
const withSaga = injectSaga({ key: 'dashboard', saga });

export default compose(
  withReducer,
  withSaga,
  withConnect,
)(Dashboard);
