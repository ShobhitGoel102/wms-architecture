/*
 *
 * Dashboard constants
 *
 */

export const DEFAULT_ACTION = 'app/Dashboard/DEFAULT_ACTION';
export const ORGANISATION_PLAN = 'app/Dashboard/ORGANISATION_PLAN';
export const ORGANISATION_PLAN_SUCCESS =
  'app/Dashboard/ORGANISATION_PLAN_SUCCESS';
export const MEMBER_STATUS = 'app/Dashboard/MEMBER_STATUS';
export const DOWNLOAD_REPORTS = 'app/Dashboard/DOWNLOAD_REPORTS';

export const UNPAID_PARKINGS = 'app/Dashboard/UNPAID_PARKINGS';
export const UNPAID_PARKINGS_SUCCESS = 'app/Dashboard/UNPAID_PARKINGS_SUCCESSs';
