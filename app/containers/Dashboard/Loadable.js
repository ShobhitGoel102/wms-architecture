/* eslint-disable import/no-unresolved */
/**
 *
 * Asynchronously loads the component for Home
 *
 */

import React from 'react';
import Loadable from 'react-loadable';
import LoadableLoading from '../../components/LoadableLoading';

export default Loadable({
  loader: () => import(/* webpackChunkName: "Home" */ './index'),
  loading: () => <LoadableLoading />,
  render: (loaded, props) => {
    const Component = loaded.default;

    return <Component {...props} />;
  },
});
