/* eslint-disable  */
import React from "react";
import cx from "classnames";
import PropTypes from "prop-types";
import { injectIntl } from "react-intl";
import { Switch, Route, Redirect } from "react-router-dom";
// creates a beautiful scrollbar
import PerfectScrollbar from "react-perfect-scrollbar";
import "react-perfect-scrollbar/dist/css/styles.css";

// @material-ui/core components
import withStyles from "@material-ui/core/styles/withStyles";

// core components
import Header from "../../library/components/Header/Header.jsx";
import Footer from "../../library/components/Footer/Footer.jsx";
import Sidebar from "./Sidebar";

import appStyle from "../../library/assets/jss/material-dashboard-pro-react/layouts/dashboardStyle.jsx";

import logo from "../../images/icon-512x512.png";
// import image from "../../images/icons/loginBackground.jpg";
import dashboardRoutes from "./dashboardRoutes";
import messages from "./messages";
import { memberSignOut } from "../App/actions";
const switchRoutes = role => (
  <Switch>
    {_.filter(dashboardRoutes, { type: role }).map((prop, key) => {
      if (prop.redirect) {
        return <Redirect from={prop.path} to={prop.pathTo} key={key} />;
      }

      if (prop.collapse)
        return prop.views.map((prop, key) => (
          <Route path={prop.path} component={prop.component} key={key} />
        ));
      return <Route path={prop.path} component={prop.component} key={key} />;
    })}
  </Switch>
);

let ps;

class Dashboard extends React.Component {
  state = {
    mobileOpen: false,
    miniActive: false
  };

  handleDrawerToggle = () => {
    this.setState({ mobileOpen: !this.state.mobileOpen });
  };

  getRoute() {
    return this.props.location.pathname !== "/maps/full-screen-maps";
  }

  componentDidMount() {
    if (navigator.platform.indexOf("Win") > -1) {
      ps = new PerfectScrollbar(this.refs.mainPanel, {
        suppressScrollX: true,
        suppressScrollY: false
      });
      document.body.style.overflow = "hidden";
    }
    if (_.isEmpty(this.props.token.roles)) {
      this.props.dispatch(memberSignOut());
    }
  }

  componentWillUnmount() {
    if (navigator.platform.indexOf("Win") > -1) {
      // ps.destroy();
    }
  }

  componentDidUpdate(e) {
    if (e.history.location.pathname !== e.location.pathname) {
      this.refs.mainPanel.scrollTop = 0;
      if (this.state.mobileOpen) {
        this.setState({ mobileOpen: false });
      }
    }
  }

  sidebarMinimize() {
    this.setState({ miniActive: !this.state.miniActive });
  }

  render() {
    const { classes, ...rest } = this.props;
    const mainPanel = `${classes.mainPanel} ${cx({
      [classes.mainPanelSidebarMini]: this.state.miniActive,
      [classes.mainPanelWithPerfectScrollbar]:
        navigator.platform.indexOf("Win") > -1
    })}`;

    const myRoutes = _.filter(dashboardRoutes, {
      type: _.get(this.props.token, "roles[0]", "")
    });

    return (
      <div className={classes.wrapper}>
        <Sidebar
          routes={myRoutes}
          logoText={this.props.intl.formatMessage(messages.WMSHeading)}
          logo={logo}
          image={null}
          handleDrawerToggle={this.handleDrawerToggle}
          open={this.state.mobileOpen}
          color="blue"
          bgColor="black"
          miniActive={this.state.miniActive}
          {...rest}
        />
        <div className={mainPanel} ref="mainPanel">
          <Header
            sidebarMinimize={this.sidebarMinimize.bind(this)}
            miniActive={this.state.miniActive}
            routes={dashboardRoutes}
            handleDrawerToggle={this.handleDrawerToggle}
            headerMessage={this.props.intl.formatMessage(
              messages.DashboardHeader
            )}
            logout={this.props.intl.formatMessage(messages.Logout)}
            logoutMessage={this.props.intl.formatMessage(
              messages.LogoutMessage
            )}
            logoutOption1={this.props.intl.formatMessage(
              messages.LogoutOption1
            )}
            logoutOption2={this.props.intl.formatMessage(
              messages.LogoutOption2
            )}
            municipalities={this.props.token.municipalities}
            {...rest}
          />
          {/* On the /maps/full-screen-maps route we want the map to be on full screen - this is not possible if the content and conatiner classes are present because they have some paddings which would make the map smaller */}
          {this.getRoute() ? (
            <div className={classes.content}>
              <div className={classes.container}>
                {switchRoutes(_.get(this.props.token, "roles[0]", ""))}
              </div>
            </div>
          ) : (
            <div className={classes.map}>
              {switchRoutes(_.get(this.props.token, "roles[0]", ""))}
            </div>
          )}
          {this.getRoute() ? (
            <Footer
              message={this.props.intl.formatMessage(messages.FooterHeader)}
            />
          ) : null}
        </div>
      </div>
    );
  }
}

Dashboard.propTypes = {
  ...Dashboard,
  classes: PropTypes.object.isRequired
};

const DashboardInitl = injectIntl(Dashboard);
export default withStyles(appStyle)(DashboardInitl);
