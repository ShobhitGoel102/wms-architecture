/*
 * Dashboard Messages
 *
 * This contains all the text for the Dashboard component.
 */

import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.containers.Dashboard.header',
    defaultMessage: 'This is Dashboard container !',
  },
  WMSHeading: {
    id: 'app.containers.Dashboard.WMSHeading',
    defaultMessage: 'WMS',
  },
  ParkingHeading: {
    id: 'app.containers.Dashboard.ParkingHeading',
    defaultMessage: 'Parking',
  },
  DashboardHeader: {
    id: 'app.containers.Dashboard.DashboardHeader',
    defaultMessage: '- WMS Dashboard',
  },
  FooterHeader: {
    id: 'app.containers.Dashboard.FooterHeader',
    defaultMessage: '- WMS - Parking Made Safe',
  },
  Logout: {
    id: 'app.containers.Dashboard.Logout',
    defaultMessage: 'Logout',
  },
  LogoutMessage: {
    id: 'app.containers.Dashboard.LogoutMessage',
    defaultMessage: 'Are you sure you want to log out?',
  },
  LogoutOption1: {
    id: 'app.containers.Dashboard.LogoutOption1',
    defaultMessage: 'No',
  },
  LogoutOption2: {
    id: 'app.containers.Dashboard.LogoutOption2',
    defaultMessage: 'Log out now',
  },
});
