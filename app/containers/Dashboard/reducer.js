/*
 *
 * Dashboard reducer
 *
 */

import { fromJS } from 'immutable';
import {
  DEFAULT_ACTION,
  ORGANISATION_PLAN_SUCCESS,
  UNPAID_PARKINGS_SUCCESS,
} from './constants';

export const initialState = fromJS({});

function dashboardReducer(state = initialState, action) {
  switch (action.type) {
    case DEFAULT_ACTION:
      return state;
    case ORGANISATION_PLAN_SUCCESS:
      return state.set('OrganisatonPlans', action.organisationPlansResponse);
    case UNPAID_PARKINGS_SUCCESS:
      return state.set('OrganisationPlans', action.count);
    default:
      return state;
  }
}

export default dashboardReducer;
