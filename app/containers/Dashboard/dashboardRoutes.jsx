// import Apps from '@material-ui/icons/Apps';
import Contacts from '@material-ui/icons/Contacts';
import EmergencyDashboard from 'containers/EmergencyDashboard';

const dashRoutes = [
  {
    path: '/Emergency',
    name: 'Emergency',
    state: 'openEmergency',
    icon: Contacts,
    component: EmergencyDashboard,
  }
];

export default dashRoutes;
