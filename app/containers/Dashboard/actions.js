/*
 *
 * Dashboard actions
 *
 */

import {
  DEFAULT_ACTION,
  ORGANISATION_PLAN,
  ORGANISATION_PLAN_SUCCESS,
  MEMBER_STATUS,
  DOWNLOAD_REPORTS,
  UNPAID_PARKINGS,
  UNPAID_PARKINGS_SUCCESS,
} from './constants';

export function defaultAction() {
  return {
    type: DEFAULT_ACTION,
  };
}
export function getOrganisationPlan() {
  return {
    type: ORGANISATION_PLAN,
  };
}
export function getOrganisationPlanSuccess(organisationPlansResponse) {
  return {
    type: ORGANISATION_PLAN_SUCCESS,
    organisationPlansResponse,
  };
}

export function getMember() {
  return {
    type: MEMBER_STATUS,
  };
}

export function downloadReports(url) {
  return {
    type: DOWNLOAD_REPORTS,
    url,
  };
}

export function unPaidParking() {
  return {
    type: UNPAID_PARKINGS,
  };
}

export function unPaidParkingSuccess(count) {
  return {
    type: UNPAID_PARKINGS_SUCCESS,
    count,
  };
}
