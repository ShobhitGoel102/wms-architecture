/*
 *
 * SnackBarNoty reducer
 *
 */
import produce from 'immer';
import { DEFAULT_ACTION, SNACKBAR_SHOW, SNACKBAR_HIDE } from './constants';

export const initialState = {
  showBar: false,
  message: '',
  variant: 'info',
};

/* eslint-disable default-case, no-param-reassign */
const snackBarNotyReducer = (state = initialState, action) =>
  produce(state, draft => {
    switch (action.type) {
      case DEFAULT_ACTION:
        break;
      case SNACKBAR_SHOW:
        draft.showBar = true;
        draft.message = action.message;
        draft.variant = action.variant;
        break;
      case SNACKBAR_HIDE:
        draft.showBar = false;
        break;
    }
  });

export default snackBarNotyReducer;
