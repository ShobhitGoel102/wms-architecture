/**
 *
 * Asynchronously loads the component for SnackBarNoty
 *
 */

import loadable from 'utils/loadable';

export default loadable(() => import('./index'));
