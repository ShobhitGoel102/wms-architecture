/**
 *
 * SnackBarNoty
 *
 */

import React, { memo } from 'react';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';

import AddAlert from '@material-ui/icons/AddAlert';

import { useInjectReducer } from '../../utils/injectReducer';
import { useInjectSaga } from '../../utils/injectSaga';
import {
  makeSelectSnackBarNoty,
  makeSelectShowSnackBarNoty,
  makeSelectSnackBarNotyMessage,
  makeSelectSnackBarNotyVariant,
} from './selectors';
import reducer from './reducer';
import saga from './saga';
import { hideNoty } from './actions';
import Snackbar from '../../library/components/Snackbar/Snackbar';

export function SnackBarNoty({ show, message, hide }) {
  useInjectReducer({ key: 'snackBarNoty', reducer });
  useInjectSaga({ key: 'snackBarNoty', saga });

  const [open, setOpen] = React.useState(false);

  if (show && !open) {
    setOpen(true);
  }

  if (!show && open) {
    setOpen(false);
  }

  return (
    <div>
      <Snackbar
        place="tr"
        color="primary"
        icon={AddAlert}
        message={message}
        open={open}
        closeNotification={() => hide()}
        close
      />
    </div>
  );
}

SnackBarNoty.propTypes = {
  ...SnackBarNoty,
};

const mapStateToProps = createStructuredSelector({
  snackBarNoty: makeSelectSnackBarNoty(),
  show: makeSelectShowSnackBarNoty(),
  message: makeSelectSnackBarNotyMessage(),
  variant: makeSelectSnackBarNotyVariant(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
    hide: () => {
      dispatch(hideNoty());
    },
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(
  withConnect,
  memo,
)(SnackBarNoty);
