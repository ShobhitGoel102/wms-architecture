/*
 * SnackBarNoty Messages
 *
 * This contains all the text for the SnackBarNoty container.
 */

import { defineMessages } from 'react-intl';

export const scope = 'app.containers.SnackBarNoty';

export default defineMessages({
  header: {
    id: `${scope}.header`,
    defaultMessage: 'This is the SnackBarNoty container!',
  },
});
