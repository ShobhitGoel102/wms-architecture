/*
 *
 * SnackBarNoty constants
 *
 */

export const DEFAULT_ACTION = 'app/SnackBarNoty/DEFAULT_ACTION';
export const SNACKBAR_SHOW = 'app/SnackBarNoty/SNACKBAR_SHOW';
export const SNACKBAR_HIDE = 'app/SnackBarNoty/SNACKBAR_HIDE';
