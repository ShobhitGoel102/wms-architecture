import { createSelector } from 'reselect';
import { initialState } from './reducer';

/**
 * Direct selector to the snackBarNoty state domain
 */

const selectSnackBarNotyDomain = state => state.snackBarNoty || initialState;

/**
 * Other specific selectors
 */

/**
 * Default selector used by SnackBarNoty
 */

const makeSelectSnackBarNoty = () =>
  createSelector(
    selectSnackBarNotyDomain,
    substate => substate,
  );

const makeSelectShowSnackBarNoty = () =>
  createSelector(
    selectSnackBarNotyDomain,
    substate => substate.showBar,
  );

const makeSelectSnackBarNotyMessage = () =>
  createSelector(
    selectSnackBarNotyDomain,
    substate => substate.message,
  );
const makeSelectSnackBarNotyVariant = () =>
  createSelector(
    selectSnackBarNotyDomain,
    substate => substate.variant,
  );
export default makeSelectSnackBarNoty;
export {
  selectSnackBarNotyDomain,
  makeSelectSnackBarNoty,
  makeSelectShowSnackBarNoty,
  makeSelectSnackBarNotyMessage,
  makeSelectSnackBarNotyVariant,
};
