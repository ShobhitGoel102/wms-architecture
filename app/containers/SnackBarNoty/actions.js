/*
 *
 * SnackBarNoty actions
 *
 */

import { DEFAULT_ACTION, SNACKBAR_HIDE, SNACKBAR_SHOW } from './constants';

export function defaultAction() {
  return {
    type: DEFAULT_ACTION,
  };
}

export function showNoty(message, variant) {
  return {
    type: SNACKBAR_SHOW,
    message,
    variant,
  };
}

export function hideNoty() {
  return {
    type: SNACKBAR_HIDE,
  };
}
