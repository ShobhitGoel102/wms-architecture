/* eslint-disable no-param-reassign */
import axios from 'axios';
import { configuredStore } from '../configureStore';
import { memberSignOut } from '../containers/App/actions';

import { showNoty, hideNoty } from '../containers/SnackBarNoty/actions';

export const axiosInstance = axios.create({
  baseURL: config.baseURL, //eslint-disable-line
  responseType: 'json',
});

axiosInstance.interceptors.request.use(
  config => {
    const { token } = configuredStore.getState().SignIn;

    if (token && token.memberId && token.id) {
      config.headers.Authorization = token.id;
      config.headers.id = token.memberId;
    }

    config.headers.deviceType = 'web';
    return config;
  },
  err => Promise.reject(err),
);

axiosInstance.interceptors.response.use(
  response => {
    if (response.data) return response.data;
    return response;
  },
  err => {
    if (!err.response) {
      configuredStore.dispatch(
        showNoty('Der gik desværre noget galt', 'error'),
      );

      setTimeout(() => {
        configuredStore.dispatch(hideNoty());
      }, 2000);
    }

    if (err.response.status === 401) {
      configuredStore.dispatch(memberSignOut());
    }

    configuredStore.dispatch(showNoty(err.response.data.message));

    setTimeout(() => {
      configuredStore.dispatch(hideNoty());
    }, 2000);

    return Promise.reject(err);
  },
);

/**
 * Requests a URL, returning a promise
 *
 * @param  {string} url       The URL we want to request
 * @param  {object} [options] The options we want to pass to "fetch"
 *
 * @return {object}           The response data
 */
export default function request(options) {
  return axiosInstance(options);
}
