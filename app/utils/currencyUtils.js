import numeral from 'numeral';

numeral.register('locale', 'da-dk', {
  delimiters: {
    thousands: '.',
    decimal: ',',
  },
  abbreviations: {
    thousand: 'k',
    million: 'mio',
    billion: 'mia',
    trillion: 'b',
  },
  ordinal() {
    return '.';
  },
  currency: {
    symbol: 'DKK',
  },
});

numeral.locale('da-dk');

const formatCurrency = num => {
  // eslint-disable-next-line no-param-reassign
  num = num || 0;
  const result = `${numeral(num).format('0,0.00')} DKK`;
  return result;
};

export default formatCurrency;
