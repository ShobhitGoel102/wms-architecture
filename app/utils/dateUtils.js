import moment from 'moment';

const formatDate = date => {
  if (!date) return 'N/A';

  const currentYear = moment().year();
  const dateYear = moment(date).year();

  if (currentYear !== dateYear) {
    return moment(date).format('DD MMM YYYY - HH:mm');
  }
  return moment(date).format('DD MMM  - HH:mm');
};

const actualDate = parking => {
  let parkingStartAt = new Date(parking.createdAt);
  if (!parking.isDelayed) parkingStartAt = new Date(parking.startAt);

  let parkingStoppedAt = new Date(parking.stoppedAt);

  if (!parking.stoppedAt)
    parkingStoppedAt = new Date(parking.estimatedStoppedAt);

  const diffMs = parkingStoppedAt - parkingStartAt; // milliseconds between now & Christmas
  const diffDays = Math.floor(diffMs / 86400000); // days
  const diffHrs = Math.floor((diffMs % 86400000) / 3600000); // hours
  const diffMins = Math.round(((diffMs % 86400000) % 3600000) / 60000); // minutes

  const result = `${`0${diffDays}`.slice(-2)} days ${`0${diffHrs}`.slice(
    -2,
  )} hours ${`0${diffMins}`.slice(-2)} minutes`;
  return result;
};

export default formatDate;

export { formatDate, actualDate };
