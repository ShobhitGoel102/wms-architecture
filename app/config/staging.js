module.exports = {
  baseURL: 'https://development-api.parkone.dk/v1',
  isLoggerEnabled: false,
  isLocal: false,
};
