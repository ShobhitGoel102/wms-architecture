/**
 *
 * Input
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import TextField from '@material-ui/core/TextField';
import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
// @material-ui/core components
import withStyles from '@material-ui/core/styles/withStyles';
// import PropTypes from 'prop-types';
// import styled from 'styled-components';

// @material-ui/icons
import Email from '@material-ui/icons/Email';
import LockOutline from '@material-ui/icons/Lock';

const useStyles = makeStyles(theme => ({
  textField: {
    marginLeft: theme.spacing(1),
    marginRight: theme.spacing(1),
  },
}));

const CssTextField = withStyles({
  root: {
    '& label.Mui-focused': {
      color: 'rgba(0, 0, 0, 0.54)',
    },
    '& .MuiInput-underline:after': {
      borderBottomColor: '#2f3542',
    },
    '& .MuiOutlinedInput-root': {
      '& fieldset': {
        borderColor: 'red',
      },
      '&:hover fieldset': {
        borderColor: 'yellow',
      },
      '&.Mui-focused fieldset': {
        borderColor: 'green',
      },
    },
  },
})(TextField);

function Input(props) {
  const classes = useStyles();

  return (
    <div style={{ margin: '4px' }}>
      <Grid
        style={{
          display: 'flex',
          flexDirection: 'row',
          alignItems: 'center',
          justifyContent: 'center',
          height: '100%',
          width: '100%',
        }}
      >
        <Grid item>
          {props.id === 'email' ? (
            <Email style={{ marginTop: '20px' }} />
          ) : (
            <LockOutline style={{ marginTop: '20px' }} />
          )}
        </Grid>
        <Grid item style={{ width: '100%' }}>
          <CssTextField
            id={props.id}
            label={props.label}
            type={props.type}
            placeholder={props.placeholder}
            className={classes.textField}
            error={props.meta.touched && props.meta.error}
            onChange={props.onChange}
            InputProps={props.InputProps}
            style={props.style}
            {...props.input}
          />
        </Grid>
      </Grid>
    </div>
  );
}

Input.propTypes = {
  ...Input,
  input: PropTypes.object,
  label: PropTypes.string,
  type: PropTypes.string,
  placeholder: PropTypes.string,
};

export default Input;
