/* eslint-disable  */
import React from 'react';
import classNames from 'classnames';
import PropTypes from 'prop-types';

import Close from '@material-ui/icons/Close';
import Slide from '@material-ui/core/Slide';
import Dialog from '@material-ui/core/Dialog';
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogContent from '@material-ui/core/DialogContent';
import DialogActions from '@material-ui/core/DialogActions';

// @material-ui/core components
import withStyles from '@material-ui/core/styles/withStyles';

import Button from '../../library/components/CustomButtons/Button';

import headerLinksStyle from '../../library/assets/jss/material-dashboard-pro-react/components/headerLinksStyle';

import { signOut } from '../../containers/App/actions';

const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="down" ref={ref} {...props} />;
});

class HeaderLinks extends React.Component {
  constructor(...props) {
    super(...props);
    this.logout = this.logout.bind(this);
    this.handleSmallModalOpen = this.handleSmallModalOpen.bind(this);
  }

  logout() {
    this.setState({
      smallModal: true,
    });

    // this.props.handleDrawerToggle();
  }

  state = {
    open: false,
  };

  handleClick = () => {
    this.setState({ open: !this.state.open });
  };

  handleClose = () => {
    this.setState({ open: false });
  };

  handleSmallModalOpen() {
    this.setState({
      smallModal: true,
    });

    // this.props.handleDrawerToggle();
  }

  handleSmallModalClose() {
    this.setState({
      smallModal: false,
    });
  }

  render() {
    const { classes, rtlActive } = this.props;

    const dropdownItem = `${classes.dropdownItem} ${classNames({
      //eslint-disable-line
      [classes.dropdownItemRTL]: rtlActive,
    })}`;
    const wrapper = classNames({
      [classes.wrapperRTL]: rtlActive,
    });
    const managerClasses = classNames({
      //eslint-disable-line
      [classes.managerClasses]: true,
    });
    return (
      <div className={wrapper}>
        <Button
          color="primary"
          style={{ marginLeft: '6%' }}
          onClick={this.logout}
        >
          Logout
        </Button>

        <Dialog
          classes={{
            root: classes.center + ' ' + classes.modalRoot,
            paper: classes.modal + ' ' + classes.modalSmall,
          }}
          open={this.state.smallModal}
          TransitionComponent={Transition}
          keepMounted
          onClose={() => this.handleSmallModalClose()}
          aria-labelledby="small-modal-slide-title"
          aria-describedby="small-modal-slide-description"
        >
          <DialogTitle
            id="small-modal-slide-title"
            disableTypography
            className={classes.modalHeader}
          >
            Log out
          </DialogTitle>
          <DialogContent
            id="small-modal-slide-description"
            className={classes.modalBody + ' ' + classes.modalSmallBody}
          >
            <h5 style={{ fontWeight: '100' }}>
              Are you sure you want to do this?
            </h5>
          </DialogContent>
          <DialogActions
            className={classes.modalFooter + ' ' + classes.modalFooterCenter}
          >
            <Button
              onClick={() => this.handleSmallModalClose()}
              color="transparent"
              className={classes.modalSmallFooterFirstButton}
            >
              Never Mind
            </Button>
            <Button
              onClick={() => this.handleSmallModalYesClose()}
              color="success"
              simple
              className={
                classes.modalSmallFooterFirstButton +
                ' ' +
                classes.modalSmallFooterSecondButton
              }
            >
              Yes
            </Button>
          </DialogActions>
        </Dialog>
      </div>
    );
  }
}

HeaderLinks.propTypes = {
  classes: PropTypes.object.isRequired,
  rtlActive: PropTypes.bool,
  dispatch: PropTypes.func,
};

export default withStyles(headerLinksStyle)(HeaderLinks);
