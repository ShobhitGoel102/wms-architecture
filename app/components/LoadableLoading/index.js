/**
 *
 * Loading
 *
 */

import React from 'react';

/* eslint react/prefer-stateless-function: 0 */
class LoadableLoading extends React.PureComponent {
  render() {
    return (
      <div
        style={{
          display: 'flex',
          justifyContent: 'center',
          alignItems: 'center',
          width: '100vw',
          height: '100vh',
          backgroundImage:
            'linear-gradient(345deg, rgb(67, 9, 108), rgb(85 , 0, 170) 49%, rgb(111, 0, 224))',
        }}
      >
        <div className="lds-heart">
          <div />
        </div>
      </div>
    );
  }
}

LoadableLoading.propTypes = {};

export default LoadableLoading;
