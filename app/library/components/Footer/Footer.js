import React from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';

// @material-ui/core components
import withStyles from '@material-ui/core/styles/withStyles';

import footerStyle from '../../assets/jss/material-dashboard-pro-react/components/footerStyle';

function Footer({ ...props }) {
  const { classes, fluid, white } = props;
  const container = cx({
    [classes.container]: !fluid,
    [classes.containerFluid]: fluid,
    [classes.whiteColor]: white,
  });
  const anchor =
    classes.a +
    cx({
      [` ${classes.whiteColor}`]: white,
    });

  return (
    <footer className={classes.footer}>
      <div className={container}>
        <p
          className={classes.right}
          style={{
            fontFamily: '"Roboto", "Helvetica", "Arial", sans-serif',
            WebkitTapHighlightColor: 'transparent',
            WebkitFontSmoothing: 'antialiased',
          }}
        >
          &copy; {1900 + new Date().getYear()}
          <a href="http://WMS.dk" className={anchor}>
            {'WMS'}
          </a>
          {' and MobilePay together make it  CHEAPER and EASIER '}
        </p>
      </div>
    </footer>
  );
}

Footer.propTypes = {
  classes: PropTypes.object.isRequired,
  fluid: PropTypes.bool,
  white: PropTypes.bool,
  rtlActive: PropTypes.bool,
};

export default withStyles(footerStyle)(Footer);
