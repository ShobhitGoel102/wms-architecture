/* eslint-disable */
import React, { useState } from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Exit from '@material-ui/icons/ExitToApp';
// @material-ui/core components
import withStyles from '@material-ui/core/styles/withStyles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Hidden from '@material-ui/core/Hidden';
import Dialog from '@material-ui/core/Dialog';
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogContent from '@material-ui/core/DialogContent';
import DialogActions from '@material-ui/core/DialogActions';
import Close from '@material-ui/icons/Close';
// material-ui icons
import Menu from '@material-ui/icons/Menu';
import MoreVert from '@material-ui/icons/MoreVert';
import ViewList from '@material-ui/icons/ViewList';
import Slide from '@material-ui/core/Slide';
// core components
// import HeaderLinks from './HeaderLinks.jsx';
import Button from '../CustomButtons/Button';
import { municipality as setSelectedMunicipality, memberSignOut } from '../../../containers/App/actions';

import headerStyle from '../../assets/jss/material-dashboard-pro-react/components/headerStyle';


const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="down" ref={ref} {...props} />;
});

function Header({ ...props }) {
  function makeBrand() {
    var name;
    name = `${_.toUpper(props.token.municipality)}${props.headerMessage}`;
    return name;
  }
  const { classes, color, rtlActive, municipalities } = props;
  const appBarClasses = cx({
    [' ' + classes[color]]: color,
  });
  const sidebarMinimize =
    classes.sidebarMinimize +
    ' ' +
    cx({
      [classes.sidebarMinimizeRTL]: rtlActive,
    });

  const [municipality, setMunicipality] = useState(municipalities[0]);
  const [smallModal, setSmallModal] = useState(false);

  props.dispatch(setSelectedMunicipality(municipality));

  return (
    <AppBar className={classes.appBar + appBarClasses}>
      <Toolbar className={classes.container}>
        <Hidden smDown>
          <div className={sidebarMinimize}>
            {props.miniActive ? (
              <Button
                justIcon
                round
                color="white"
                onClick={props.sidebarMinimize}
              >
                <ViewList className={classes.sidebarMiniIcon} />
              </Button>
            ) : (
                <Button
                  justIcon
                  round
                  color="white"
                  onClick={props.sidebarMinimize}
                >
                  <MoreVert className={classes.sidebarMiniIcon} />
                </Button>
              )}
          </div>
        </Hidden>
        <div className={classes.flex}>
          {/* Here we create navbar brand, based on route name */}
          <div className="d-flex flex-row align-items-center justify-content-between" color="transparent">
            <div className="d-flex flex-row align-items-center justify-content-start" style={{ marginLeft: '10px' }}>
              <FormControl style={{ marginRight: '10px' }}>
                <Select
                  MenuProps={{
                    className: classes.selectMenu,
                  }}
                  classes={{
                    select: classes.select,
                  }}
                  value={municipality}
                  onChange={(event) => setMunicipality(event.target.value)}
                  inputProps={{
                    name: 'simpleSelect',
                    id: 'simple-select',
                  }}
                  style={{
                    border: '1px solid #2f3542',
                    borderRadius: '5px',
                    padding: '7px',
                    backgroundColor: '#ffffff',
                  }}
                  disableUnderline
                >
                  {_.map(municipalities, name => (
                    <MenuItem
                      classes={{
                        root: classes.selectMenuItem,
                        selected: classes.selectMenuItemSelected,
                      }}
                      value={name}
                      key={name}
                    >
                      {_.capitalize(name)}
                    </MenuItem>
                  ))}

                </Select>
              </FormControl>

              {makeBrand()}

            </div>
            <div style={{ marginRight: '2%' }}><Button color="primary" onClick={() => setSmallModal(true)}><Exit /></Button></div>
          </div>
        </div>
        <Hidden mdUp>
          <Button
            className={classes.appResponsive}
            color="transparent"
            justIcon
            aria-label="open drawer"
            onClick={props.handleDrawerToggle}
          >
            <Menu />
          </Button>
        </Hidden>
      </Toolbar>

      <Dialog
        classes={{
          root: classes.center + ' ' + classes.modalRoot,
          paper: classes.modal + ' ' + classes.modalSmall,
        }}
        open={smallModal}
        TransitionComponent={Transition}
        keepMounted
        onClose={() => setSmallModal(false)}
        aria-labelledby="small-modal-slide-title"
        aria-describedby="small-modal-slide-description"
      >
        <DialogTitle
          id="small-modal-slide-title"
          disableTypography
          className={classes.modalHeader}
        >
          {props.logout}
        </DialogTitle>
        <DialogContent
          id="small-modal-slide-description"
          className={classes.modalBody + ' ' + classes.modalSmallBody}
        >
          <h5 style={{ fontWeight: '100' }}>
            {props.logoutMessage}
          </h5>
        </DialogContent>
        <DialogActions
          className={classes.modalFooter + ' ' + classes.modalFooterCenter}
        >
          <Button

            color="transparent"
            className={classes.modalSmallFooterFirstButton}
            onClick={() => setSmallModal(false)}
          >
            {props.logoutOption1}
          </Button>
          <Button
            onClick={() => { setSmallModal(false); props.dispatch(memberSignOut()) }}
            color="success"
            simple
            className={
              classes.modalSmallFooterFirstButton +
              ' ' +
              classes.modalSmallFooterSecondButton
            }
          >
            {props.logoutOption2}
          </Button>
        </DialogActions>
      </Dialog>

    </AppBar>
  );
}

Header.propTypes = {
  classes: PropTypes.object.isRequired,
  color: PropTypes.oneOf(['primary', 'info', 'success', 'warning', 'danger']),
  rtlActive: PropTypes.bool,
};

export default withStyles(headerStyle)(Header);
